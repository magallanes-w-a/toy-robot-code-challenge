package toy.robot.code.challenge.main;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import toy.robot.code.challenge.constant.Command;
import toy.robot.code.challenge.constant.Direction;
import toy.robot.code.challenge.model.Position;

public class Main {
	
	public static void main(String[] args) {
		Main main = new Main();
		main.initializeToyRobot(main);
	}
	
	private void initializeToyRobot(Main main) {
		System.out.println("Input Robot Position \"PLACE X,Y,F\":");
        Scanner scanner = new Scanner(System.in);
        
        boolean isRobotPlaced = false;
        
        while (!isRobotPlaced) {
        	
        	String robotPosition = scanner.nextLine();
        	
        	if (robotPosition.toUpperCase().contains(Command.PLACE)) {
        		Position position = validateAndMapPosition(robotPosition, null);
        		
        		if (Objects.nonNull(position)) {
        			if (isValidPosition(position)) {        				
        				isRobotPlaced = true;
        				main.executeCommand(scanner, position);
        			} else {
        				System.out.println("Coordinates must only be from 0,0 upto 5,5");
        			}
        		} else {
        			System.out.println("Invalid \"PLACE\" command! ex: PLACE 0,0,NORTH");
        		}
        	} else {
        		System.out.println("You can only use the \"PLACE\" for the first command ex: PLACE 0,0,NORTH");
        	}
        }
        
        scanner.close();
	}

	private void executeCommand(Scanner scanner, Position position) {
		boolean isCompleted = false;
		
		System.out.println("Input Command (PLACE X,Y,F, MOVE, LEFT, RIGHT, REPORT):");
		while (!isCompleted) {
			String command = scanner.nextLine();
			
			if (command.toUpperCase().contains(Command.PLACE)) {
				position = validateAndMapPosition(command, null);
			} else {				
				switch (command.toUpperCase()) {
				case Command.MOVE:
					position = movePosition(position);
					break;
				case Command.LEFT:
				case Command.RIGHT:
					position.setDirection(mapDirection(position.getDirection(), command.toUpperCase()));
					break;
				case Command.REPORT:
					isCompleted = true;
					System.out.println("Output: " + position.getX() + "," + position.getY() + "," + position.getDirection());
					break;
				default:
					System.out.println("Invalid command! valid commands are: \"PLACE X,Y,F, MOVE, LEFT, RIGHT, REPORT\"");
				}
			}
			
		}
	}

	private Position movePosition(Position position) {
		Position tempPosition = new Position();
		tempPosition.setDirection(position.getDirection());
		tempPosition.setX(position.getX());
		tempPosition.setY(position.getY());
		
		switch (position.getDirection()) {
			case Direction.NORTH:
				tempPosition.setY(tempPosition.getY() + 1);
				if (isValidPosition(tempPosition)) {
					position.setY(tempPosition.getY());
				}
				break;
			case Direction.SOUTH:
				tempPosition.setY(tempPosition.getY() - 1);
				if (isValidPosition(tempPosition)) {
					position.setY(tempPosition.getY());
				}
				break;
			case Direction.WEST:
				tempPosition.setX(tempPosition.getX() - 1);
				if (isValidPosition(tempPosition)) {
					position.setX(tempPosition.getX());
				}
				break;
			case Direction.EAST:
				tempPosition.setX(tempPosition.getX() + 1);
				if (isValidPosition(tempPosition)) {
					position.setX(tempPosition.getX());
				}
				break;
		}
		return position;
	}

	private Position validateAndMapPosition(String robotPosition, String movement) {
		if (robotPosition.toUpperCase().contains(Command.PLACE + " ")) {
			String coordinates = robotPosition.toUpperCase().replaceAll(Command.PLACE + " ", "");
			
			List<String> coordinatesList = Arrays.asList(coordinates.split(","));
			
			if (coordinatesList.size() == 3) {
				Integer x = parsePosition(coordinatesList.get(0));
				Integer y = parsePosition(coordinatesList.get(1));
				String direction = mapDirection(coordinatesList.get(2), movement);
				
				if (Objects.nonNull(x) && Objects.nonNull(y) && Objects.nonNull(direction)) {
					Position position = new Position();
					position.setX(x);
					position.setY(y);
					position.setDirection(direction);
					
					return position;
				}
			}
			
			return null;
		} else {
			return null;
		}
	}
	
	private Integer parsePosition(String num) {
		try {
			return Integer.parseInt(num);
		} catch (NumberFormatException e) {
		    return null;
		}
	}

	List<String> coordinatesList = Arrays.asList(Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST);
	
	private String mapDirection(String direction, String movement) {
		if (Objects.nonNull(movement)) {
			int index = coordinatesList.indexOf(direction);
			
			if (movement.equalsIgnoreCase(Command.RIGHT)) {
				index++;
			} else {
				index--;
			}
			
			if (index < 0) {
				index = 3;
			}
			
			if (index > 3) {
				index = 0;
			}
			
			return coordinatesList.get(index);
		} else {
			return coordinatesList.contains(direction) ? direction.toUpperCase() : null;
		}
	}
	
	private boolean isValidPosition(Position position) {
		if (position.getX() >= 0 && position.getX() < 6 && position.getY() >= 0 && position.getY() < 6) {
			return true;
		} else {
			return false;
		}
	}
}
