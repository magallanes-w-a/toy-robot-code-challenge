package toy.robot.code.challenge.constant;

public class Command {
	public static final String PLACE = "PLACE";
	public static final String MOVE = "MOVE";
	public static final String LEFT = "LEFT";
	public static final String RIGHT = "RIGHT";
	public static final String REPORT = "REPORT";
}
